'use strict'


let http = require('http')

let Articles = require('./utils/Articles')
let articlesData = require('./utils/articles_data')

let articles = new Articles(articlesData)
//handlers 
let home = require('./handlers/home')
let list = require('./handlers/list')
let create = require('./handlers/create')
let details = require('./handlers/details')
let images = require('./handlers/images')
let comments = require('./handlers/comments')


let server = function (req, res) {
    let handlers = [create, home, list, details, images, comments];

    for (let handler of handlers) {
        let next = handler (req, res, articles)
        if (!next) break;
    }
} 

http.createServer(server).listen(8000)