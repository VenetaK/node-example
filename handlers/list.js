'use strict'

let UrlManager = require('./../utils/UrlManager')
let urlManager = new UrlManager();
let renderView = require('./../utils/renderFile')

let listHandler = function (req, res, articles) {
    let friendlyUrlObj = urlManager.getFriendlyUrl(req)
 
    if(friendlyUrlObj.url === '/all'){
        renderView('list', {articles: articles.getArticles()}, res)
    } else {
        return true 
    }
}

module.exports = listHandler