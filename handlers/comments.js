'use strict'

let UrlManager = require('./../utils/UrlManager')
let urlManager = new UrlManager();
let renderView = require('./../utils/renderFile')
let querystring = require('querystring')

let commentsHandler = function (req, res, articles) {
    let friendlyUrlObj = urlManager.getFriendlyUrl(req)
    let article = articles.where('id', friendlyUrlObj.id)[0]

    if(friendlyUrlObj.url === '/comments' && req.method === 'GET'){
        renderView('comments', {}, res)
    } else if (friendlyUrlObj.url === '/comments' && req.method === 'POST') {
        let commentData = ''
        req.on('data', (data) => {
            commentData += data
        })
        req.on('end', () => {
            let comment = querystring.parse(commentData)
            comment.created = new Date().toString()
             
            article.comments.push(comment)
            res.writeHead(302, {
                'Location': '/all/'+ article.id + '/details'
            })
            renderView('details', {article: article}, res)
        })
    } else {
        return true
    }
}

module.exports = commentsHandler