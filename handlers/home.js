'use strict'

let UrlManager = require('./../utils/UrlManager')
let urlManager = new UrlManager();
let renderView = require('./../utils/renderFile')

let homeHandler = function (req, res, articles) {
    let friendlyUrlObj = urlManager.getFriendlyUrl(req)

    if(friendlyUrlObj.url === '/'){
        renderView('home', {articles: articles.where('deleted', false)}, res)
    } else {
        return true
    }
}

module.exports = homeHandler