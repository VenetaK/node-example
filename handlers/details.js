'use strict'
  
let UrlManager = require('./../utils/UrlManager')
let urlManager = new UrlManager();
let renderView = require('./../utils/renderFile')
 
let detailsHandler = function (req, res, articles) {
    let friendlyUrlObj = urlManager.getFriendlyUrl(req)
    let article = articles.where('id', friendlyUrlObj.id)[0]

    if (friendlyUrlObj.url === 'details' && req.method === 'GET') {
        renderView('details', {article: article}, res)
    } else if (friendlyUrlObj.url === 'details' && req.method === 'POST'){
        req.on('data', () => {})
        req.on('end', () => {
            article.deleted = article.deleted?false:true //toggle deleted prop
            renderView('details', {article: article}, res)
        })
    } else {
        return true
    } 
}

module.exports = detailsHandler