'use strict'

let url = require('url')


let formidable = require('formidable')

let UrlManager = require('./../utils/UrlManager')
let urlManager = new UrlManager();
let renderView = require('./../utils/renderFile')

let createHandler = function (req, res, articles) {
    let friendlyUrlObj = urlManager.getFriendlyUrl(req)

    if (friendlyUrlObj.url === '/create' && req.method === 'GET') {
        renderView('create', {}, res)
    } else if (friendlyUrlObj.url === '/create' && req.method === 'POST') {
            let form = new formidable.IncomingForm()
            form.encoding = 'utf-8';
            let result = {}
            let images = [];

            form.on('fileBegin', (name, file) => {
                let imgName = Date.now() + '_' + file.name
                images.push(imgName)
                file.path = 'images/' + imgName
            })
            form.on('field', function (name, value) {
                result[name] = value
            })
            form.on('end', function () {
                result.images = images
                result.id = Date.now() 

                articles.add(result)

                res.writeHead(302, {
                    'Location': '/all'
                })
                renderView('list', { articles: articles.getArticles() }, res)
            })

            form.parse(req)
    } else {
        return true
    }
}

module.exports = createHandler