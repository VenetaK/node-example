'use strict'

let url = require('url')
let fs = require('fs')
let UrlManager = require('./../utils/UrlManager')
let urlManager = new UrlManager();

let imagesHandler = function (req, res, articles) {
    let friendlyUrlObj = urlManager.getFriendlyUrl(req)

    // console.log(url.parse(req.url).pathname)

    if (friendlyUrlObj.url === '/images') {
        let pathName = (url.parse(req.url).pathname).toString()
        fs.readFile(__dirname + '/./..' + pathName, (err, file) => {
            if (err) console.log(err)
            res.writeHead(200) 
            res.write(file)
            res.end()
        })
    } else {
        return true
    }
}

module.exports = imagesHandler