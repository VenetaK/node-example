'use strict'

let Articles = function (articlesData) {
    this.articles = articlesData
}

Articles.prototype.getArticles = function () {
    return this.articles;
}

Articles.prototype.where = function (propName, propVal) {
    let matchingItems = []
    for (let article of this.articles){
        if (article[propName] == propVal) matchingItems.push(article) 
    }
    return matchingItems
}

Articles.prototype.setDefaults = function (data) {
    data.comments = []
    data.deleted = false
    data.created = Date.now()
    data.views = 1

    return data
}

Articles.prototype.add = function (data) {
    this.articles.push( this.setDefaults(data))
}

module.exports = Articles