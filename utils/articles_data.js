'use strict'

let articlesData = [
    {
            name: 'Test',
            id: 123213213,
            text: 'asddadasdasd_0',
            created: 1476015228114,
            deleted: true,
            views: 6,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created: 1476015228114
                }
            ],
            images: []
        },
        {
            name: 'Test_1',
            id: 123219903213,
            text: 'asddadasdasd_1',
            created: 1476015228114,
            deleted: true,
            views: 5,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created: 1476015228114

                }
            ],
            images: []
        },
        {
            name: 'Test_2',
            id: 1239098213213,
            text: 'asddadasdasd_2',
            created: 1476015228114,
            deleted: false,
            views: 4,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created: 1476015228114
                }
            ],
            images: []
        },
        {
            name: 'Test_3',
            id: 123212233213,
            text: 'asddadasdasd_3',
            created: 1476015228114,
            deleted: false,
            views: 3,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created: 1476015228114
                }
            ],
            images: []
        },
        {
            name: 'Test_4',
            id: 123212255633213,
            text: 'asddadasdasd_4',
            created: 1476015228114,
            deleted: false,
            views: 2,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created:1476015228114
                }
            ],
            images: []
        },
        {
            name: 'Test_5',
            id: 12321223398987213,
            text: 'asddadasdasd_5',
            created: 1476015228114,
            deleted: false,
            views: 1,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created:1476015228114
                }
            ],
            images: []
        },
        {
            name: 'Test_6',
            id: 12321226633213,
            text: 'asddadasdasd_6',
            created: 1476015228114,
            deleted: false,
            views: 0,
            comments: [
                {
                    author: 'veneta@veneta.com',
                    text: 'test test test',
                    created: 1476015228114
                }
            ],
            images: []
        }
]

module.exports = articlesData