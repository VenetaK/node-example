'use strict'

let url = require('url')

let UrlManager = function () {}

UrlManager.prototype.getFriendlyUrl = function (req) {
    let friendlyUrlObj = {
        url: '/',
        id: null
    } 
    let parsedUrl = url.parse(req.url)            
    let urlPaths = parsedUrl.pathname.split('/')
    if(parsedUrl.pathname.toString().match(/\/images/)) {
        friendlyUrlObj.url = '/images' 
        friendlyUrlObj.id = null
    } else {
        switch (urlPaths.length) {
        case 5:
            friendlyUrlObj.url = '/comments'
            friendlyUrlObj.id = urlPaths[2]
            break
        case 4:
            friendlyUrlObj.url = 'details'
            friendlyUrlObj.id =urlPaths[2]
            break
        case 2:
            friendlyUrlObj.url = parsedUrl.pathname
            friendlyUrlObj.id = null 
            break
        default:
            friendlyUrlObj.url = parsedUrl.pathname
            friendlyUrlObj.id = null
            break
        }

    }

    return friendlyUrlObj;
}

module.exports = UrlManager