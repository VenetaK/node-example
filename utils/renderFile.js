'use strict'

let ejs = require('ejs')

let renderFile = function (name, data, res) {
    ejs.renderFile(__dirname + '/./../views/' + name + '.html', data, {}, function (err, str) {
        if (err) {
            console.log(err)
            res.writeHead(404)
            res.write('404 Not found!')
            res.end()
        } else {
            // res.writeHead(200)
            res.write(str)
            res.end()
        }
    }
)}

module.exports = renderFile

